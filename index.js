const fs = require('fs');
const archiver = require('archiver');
const json = require('./sal_data.json'); // Replace this with your JSON data
const path = require('path');
const outputDir = './mySCORMPackage/';
const outputDirContent = `${outputDir}course_content/`;



// css file
// const sourcePath = 'style.css'; // Replace with the path to your source file
const sharedPaths = ['common_shared/cclicense.png', 'common_shared/background.jpg', 'common_shared/style.css', 'common_shared/contentfunctions.js', 'common_shared/scormfunctions.js', 'common_shared/assessmenttemplate.html'];
const sourcePaths = ['common_shared/xml/adlcp_rootv1p2.xsd', 'common_shared/xml/ims_xml.xsd', 'common_shared/xml/imscp_rootv1p1p2.xsd', 'common_shared/xml/imsmd_rootv1p2p1.xsd']
const contentPaths = ['common_shared/play.jpg', 'common_shared/goodbye.html']
const destinationFolder = `${outputDir}`; // Replace with the path to your destination folder
const destinationSharedFolder = `./mySCORMPackage/shared`; // Replace with the path to your destination folder
const destinationContentFolder = `./mySCORMPackage/course_content`; // Replace with the path to your destination folder

// Function to copy a file
function copyFile(source, destination) {
  fs.copyFile(source, destination, (err) => {
    if (err) {
      console.error('Error copying file:', err);
    } else {
      console.log(`File copied from ${source} to ${destination} successfully.`);
    }
  });
}

fs.mkdirSync(outputDirContent, { recursive: true });

// Map block types to HTML tags
const blockTypeMap = {
  paragraph: 'p',
  header: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
  image: 'img'
};

let index = 1; // For item numbering in SCORM manifest
const manifestResources = []; // Store resource details for manifest

// Process each block in the JSON
json.blocks.forEach(block => {
  const type = block.type;
  const blockData = block.data;

  let htmlContent = '';
  let tagName = '';

  // Mapping block type to HTML tags
  if (blockTypeMap.hasOwnProperty(type)) {
    if (Array.isArray(blockTypeMap[type])) {
      tagName = blockTypeMap[type][blockData.level - 1]; // For headers
    } else {
      tagName = blockTypeMap[type];
    }
  }

  // Generate HTML content based on block type
  if (tagName) {
    if (type === "quiz") {

    }
    else if (type === 'image') {
      htmlContent = `<${tagName} src="${blockData.file.url}" alt="${blockData.file.name}"/>`;
    } else {
      htmlContent = `<${tagName}>${blockData.text}</${tagName}>`;
    }
  }

  const htmlFile = `
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style type="text/css" media="screen">
    @import url( ../shared/style.css );
  </style>
<script src="../shared/scormfunctions.js" type="text/javascript"></script>
<script src="../shared/contentfunctions.js" type="text/javascript"></script>
</head>
<body>
  <div class="sample-header">
  <img src="play.jpg" id="golfimg" alt="golfing image" />
  <div class="sample-header-section">
    <h1>Scroll down to see the parallax effect</h1>
    <h2>Background landcape scrolls with its own depth </h2>
  </div>
</div>
<div class="sample-section-wrap">
  <div class="sample-section">
   ${htmlContent}
  </div>
</div>
<script type="text/javascript">
AddLicenseInfo("Wikipedia", "http://en.wikipedia.org/wiki/Golf_etiquette");
AddTagLine();
</script>
</body>
</html>`

  const quizFile = blockData?.questions?.map(({ id, text, type, correctAnswer, objectiveId, answers }) => {
    return `test.AddQuestion(new Question("${id}",
    "${text}",
    QUESTION_TYPE_${type},
    new Array("${answers?.join('", "')}"),
    "${correctAnswer}",
    "${objectiveId}")
);`
  })

  // Write HTML content to a file
  const filename = type === "quiz" ? `${outputDirContent}questions.js` : `${outputDirContent}section_${index}.html`;
  fs.writeFileSync(filename, type === "quiz" ? quizFile.join("\n\n") : htmlFile);

  // Store resource details for manifest
  manifestResources.push({
    id: `item_${index}`,
    type: block.type,
    href: block.type !== "quiz" ? `course_content/section_${index}.html` : `course_content/questions.js`,
    title: blockData.text || `Block ${index}`
  });

  index++;
});

// Create SCORM manifest
const manifestContent = `<?xml version="1.0" standalone="no" ?>
<!--
Multiple SCO, single file per SCO, content packaging example. SCORM 1.2.

Provided by Rustici Software - http://www.scorm.com

This example demonstrates a package containing a hierarchy of SCOs that consist of one
HTML page each. In a package like this, the LMS handles all navigation between SCOs.
An aggregation of the SCOs is created using the item hierarchy.
-->

<!-- 
The manifest node contains a unique identifer for this course and the course's version number.
The schema declartions are important to ensure you are delivering valid XML. For the most part
these should remain static. Other schema prefixes are allowed, but can limit interoperabilty.
-->
<manifest identifier="mySCORMPackage"
          version="1.0"
          xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2"
          xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd
                              http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd
                              http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd">
  <metadata>
    <schema>ADL SCORM</schema>
    <schemaversion>1.2</schemaversion>
    <title>${json.name || 'Course Title'}</title>
    <lom:lom>
      <lom:general>
          <lom:title>
            <lom:langstring>${json.name || 'Course Title'}</lom:langstring>
          </lom:title>
      </lom:general>
    </lom:lom>
  </metadata>
  <organizations default="myOrg">
    <organization identifier="myOrg">
      <title>My SCORM Content</title>
      ${manifestResources.map(resource =>
  resource.type === "quiz" ?
    `     <item identifier="${resource.id}" identifierref="etiquette_quiz_resource" parameters="?questions=course_content">
            <title>Handicapping Quiz</title>
    </item>`  :
    `     <item identifier="${resource.id}" identifierref="${resource.id}">
            <title>${resource.title}</title>
    </item>`
).join('\n')}
    </organization>
  </organizations>
  <!--
  In this example, every HTML page is a seperate resource. There is a set of common files shared by all resources.
  These files are contained in an asset that every other resource references as a dependency. Notice that these
  resources are all market as assets (vs SCOs) because these HTML files do not communicate with the LMS. This is a
  perfectly valid way of delivering SCORM conformant content.
  -->
  <resources>
    ${manifestResources.map(resource =>
  resource.type === "quiz" ?
    `<resource identifier="etiquette_quiz_resource" type="webcontent" adlcp:scormType="asset" href="shared/assessmenttemplate.html">
        <file href="${resource?.href}"/>
        <dependency identifierref="common_files" />
      </resource>` :
    `<resource identifier="${resource.id}" type="webcontent" adlcp:scormType="sco" href="${resource.href}">
        <file href="${resource.href}"/>
        <dependency identifierref="common_files" />
      </resource>`
).join('\n')}
  <resource identifier="common_files" type="webcontent" adlcp:scormtype="asset">
  <file href="shared/assessmenttemplate.html"/>
  <file href="shared/background.jpg"/>
  <file href="shared/cclicense.png"/>
  <file href="shared/contentfunctions.js"/>
  <file href="shared/launchpage.html"/>
  <file href="shared/scormfunctions.js"/>
  <file href="shared/style.css"/>
</resource>
  </resources>
</manifest>`;

// Check if the source file exists
sourcePaths.forEach((sourcePath) => {
  fs.access(sourcePath, fs.constants.F_OK, (sourceErr) => {
    if (sourceErr) {
      console.error('Source file does not exist');
    } else {
      // Check if the destination folder exists
      fs.access(destinationFolder, fs.constants.F_OK, (destErr) => {
        if (destErr) {
          // Create the destination folder if it doesn't exist
          fs.mkdirSync(destinationFolder, { recursive: true });

          // Get the file name from the source path
          const fileName = path.basename(sourcePath);
          const destinationFilePath = path.join(destinationFolder, fileName);

          // Copy the file
          copyFile(sourcePath, destinationFilePath);
        } else {
          // Get the file name from the source path
          const fileName = path.basename(sourcePath);
          const destinationFilePath = path.join(destinationFolder, fileName);

          // Copy the file
          copyFile(sourcePath, destinationFilePath);
        }
      });
    }
  });
})

// Check if the source file exists
sharedPaths.forEach((sourcePath) => {
  fs.access(sourcePath, fs.constants.F_OK, (sourceErr) => {
    if (sourceErr) {
      console.error('Source file does not exist');
    } else {
      // Check if the destination folder exists
      fs.access(destinationSharedFolder, fs.constants.F_OK, (destErr) => {
        if (destErr) {
          // Create the destination folder if it doesn't exist
          fs.mkdirSync(destinationSharedFolder, { recursive: true });

          // Get the file name from the source path
          const fileName = path.basename(sourcePath);
          const destinationFilePath = path.join(destinationSharedFolder, fileName);

          // Copy the file
          copyFile(sourcePath, destinationFilePath);
        } else {
          // Get the file name from the source path
          const fileName = path.basename(sourcePath);
          const destinationFilePath = path.join(destinationSharedFolder, fileName);

          // Copy the file
          copyFile(sourcePath, destinationFilePath);
        }
      });
    }
  });
})

// Check if the source file exists
contentPaths.forEach((sourcePath) => {
  fs.access(sourcePath, fs.constants.F_OK, (sourceErr) => {
    if (sourceErr) {
      console.error('Source file does not exist');
    } else {
      // Check if the destination folder exists
      fs.access(destinationContentFolder, fs.constants.F_OK, (destErr) => {
        if (destErr) {
          // Create the destination folder if it doesn't exist
          fs.mkdirSync(destinationContentFolder, { recursive: true });

          // Get the file name from the source path
          const fileName = path.basename(sourcePath);
          const destinationFilePath = path.join(destinationContentFolder, fileName);

          // Copy the file
          copyFile(sourcePath, destinationFilePath);
        } else {
          // Get the file name from the source path
          const fileName = path.basename(sourcePath);
          const destinationFilePath = path.join(destinationContentFolder, fileName);

          // Copy the file
          copyFile(sourcePath, destinationFilePath);
        }
      });
    }
  });
})



fs.writeFileSync('./mySCORMPackage/imsmanifest.xml', manifestContent);

const output = fs.createWriteStream('mySCORMPackage.zip');
const archive = archiver('zip', {
  zlib: { level: 9 } // Compression level
});

output.on('close', function () {
  console.log('SCORM package created successfully.');
});

archive.on('error', function (err) {
  throw err;
});

archive.pipe(output);
archive.directory('mySCORMPackage/', false);
archive.finalize();
