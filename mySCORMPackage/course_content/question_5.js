test.AddQuestion(new Question("etiquette_1",
    "When another player is attempting a shot, it is best to stand:",
    QUESTION_TYPE_CHOICE,
    new Array("On top of his ball", "Directly in his line of fire", "Out of the player's line of sight"),
    "Out of the player's line of sight",
    "obj_etiquette")
);

test.AddQuestion(new Question("etiquette_2",
    "Generally sand trap rakes should be left outside of the hazard",
    QUESTION_TYPE_TF,
    new Array("undefined"),
    "true",
    "obj_etiquette")
);

test.AddQuestion(new Question("etiquette_3",
    "The player with the best score on the previous hole tees off:",
    QUESTION_TYPE_CHOICE,
    new Array("First", "Last", "With a putter"),
    "First",
    "obj_etiquette")
);

test.AddQuestion(new Question("etiquette_4",
    "Golfer A has a course handicap of 6. Golfer B has a course handicap of 10. Golfer A shoots an 81. Golfer B shoots an 84. Golfer B wins the match by how many strokes?",
    QUESTION_TYPE_NUMERIC,
    new Array("undefined"),
    "1",
    "obj_etiquette")
);