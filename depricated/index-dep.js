const fs = require('fs');
const json = require('../json_data.json'); // Replace this with your JSON data
const archiver = require('archiver');

const outputDir = './mySCORMPackage-dep/course_content/';
fs.mkdirSync(outputDir, { recursive: true });

// Generate HTML files for each section
json.sections.forEach((section, index) => {
  const filename = `${outputDir}section_${index + 1}.html`;
  const htmlContent = `<html><head><title>${section.title}</title></head><body>${section.content}</body></html>`;
  fs.writeFileSync(filename, htmlContent);
});

const manifestContent = `<?xml version="1.0"?>
<manifest identifier="mySCORMPackage-dep"
          version="1.0"
          xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2"
          xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <metadata>
    <schema>ADL SCORM</schema>
    <schemaversion>1.2</schemaversion>
    <title>${json.title}</title>
  </metadata>
  <organizations>
    <organization identifier="myOrg">
      <title>${json.title}</title>
      ${json.sections.map((section, index) =>
  `<item identifier="item_${index + 1}" identifierref="resource_${index + 1}">
          <title>${section.title}</title>
        </item>`
).join('\n')}
    </organization>
  </organizations>
  <resources>
    ${json.sections.map((section, index) =>
  `<resource identifier="resource_${index + 1}" type="webcontent" adlcp:scormType="sco" href="course_content/section_${index + 1}.html">
        <file href="course_content/section_${index + 1}.html"/>
      </resource>`
).join('\n')}
  </resources>
</manifest>`;

fs.writeFileSync('./mySCORMPackage-dep/imsmanifest.xml', manifestContent);

const output = fs.createWriteStream('mySCORMPackage-dep.zip');
const archive = archiver('zip', {
  zlib: { level: 9 } // Compression level
});

output.on('close', function () {
  console.log('SCORM package created successfully.');
});

archive.on('error', function (err) {
  throw err;
});

archive.pipe(output);
archive.directory('mySCORMPackage-dep/', false);
archive.finalize();
